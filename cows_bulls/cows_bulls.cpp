
#include "pch.h"
#include <iostream>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void calc(int* arr, int size, int number) {
	for (int i = size - 1; i >= 0; i--) {
		arr[i] = number % 10;
		number /= 10;
	}
	return;
}

int RandPCNumber(int choose) {
	int number;
	if (choose == 4) {
		do {
			number = rand() / 10;
		} while (number < 1000);
		return number;
	}
	if (choose == 8) {
		do {
			number = rand() * 1235;
		} while (number < 10000000);
		return number;

	}
}

int cows_game(int* arr1, int* arr2, int size) {
	int * arr3 = (int *)malloc(size * sizeof(int));
	for (int i = 0; i < size; i++) {
		arr3[i] = arr2[i];
	}
	//int size = sizeof(*arr1) / sizeof(arr1[0]);
	int game_cows = 0;
	for (int i = 0; i < size; i++) {
		int j = 0;
		for (int j = 0; j < size; j++) {
			if (arr1[i] == arr3[j]) {
				game_cows++;
				arr3[j] = -1;
			}
		}
	}
	free(arr3);
	return game_cows;
}

int bulls_game(int* arr1, int* arr2, int size) {
	//int size = sizeof(*arr1) / sizeof(arr1[0]);
	int game_bulls = 0;
	for (int i = 0; i < size; i++) {
		if (arr1[i] == arr2[i])
			game_bulls++;
	}
	return game_bulls;
}


int main()
{

	setlocale(LC_CTYPE, "Russian");
	int cows, bulls, pc_number, user_number, *user_number_array, *pc_number_array, N;
	srand((unsigned int)time(0));

	printf("Будем играть в четырехзначные или восьмизначные числа?\n4 - четырехзначные; 8 - восьмизначные\nВвод: ");
	scanf_s("%d", &N);

	user_number_array = (int*)malloc(N * sizeof(int));
	pc_number_array = (int*)malloc(N * sizeof(int));

	pc_number = RandPCNumber(N);

	printf("%d\n", pc_number);

	calc(pc_number_array, N, pc_number);

	while (1) {
		printf("Введите число: ");
		scanf_s("%d", &user_number);
		calc(user_number_array, N, user_number);
		for (int i = 0; i < N; i++) { // Вывод пользовательского числа
			printf("%d, ", user_number_array[i]);
		}
		printf("\n");
		for (int i = 0; i < N; i++) { // Вывод компьютерного числа
			printf("%d, ", pc_number_array[i]);
		}
		printf("\n");
		cows = cows_game(user_number_array, pc_number_array, N);
		printf("Число коров: %d\n", cows);
		bulls = bulls_game(user_number_array, pc_number_array, N);
		printf("Число быков: %d\n", bulls);
		if (bulls == N) {
			printf("Вы выиграли!!!");
			break;
		}
	}
	free(user_number_array);
	free(pc_number_array);
}
